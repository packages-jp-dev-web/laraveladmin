<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/laraveladminhome', function () {
    return view('laraveladmin::default.home');
});
Route::get('/laraveladmintypographys', function () {
    return view('laraveladmin::typographys');
});
Route::get('/laraveladmincards', function () {
    return view('laraveladmin::cards');
});
Route::get('/laraveladminforms', function () {
    return view('laraveladmin::default.forms');
});
Route::get('/laraveladminbuttons', function () {
    return view('laraveladmin::buttons');
});
Route::get('/laraveladmintabels', function () {
    return view('laraveladmin::default.tabels');
});
Route::get('/laraveladminlogin', function () {
    return view('laraveladmin::default.login');
});

Route::get('/laraveladminerror', function () {
    return view('laraveladmin::default.error');
});