function openOrCloseMenu() {
    var selectorButtonOpen = ".open-menu";
    var classButtonOpen = "is-active";
    var selectorSidebar = ".context-sidebar",
        classOpenSidebar = "open-sidebar";
    var selectorSite = ".context-site",
        classOpenContextSite = "open-context-site";
    if ($(selectorSidebar).hasClass(classOpenSidebar)) {
        $(selectorSidebar).removeClass(classOpenSidebar);
        $(selectorSite).removeClass(classOpenContextSite);
        $(selectorButtonOpen).removeClass(classButtonOpen);
    } else {
        $(selectorSidebar).addClass(classOpenSidebar);
        $(selectorSite).addClass(classOpenContextSite);
        $(selectorButtonOpen).addClass(classButtonOpen);
    }
}


function focusInInput(event) {
    var blockinput = event.parentElement;
    blockinput.classList.add("block-input-active");
}
function focusOutInput(event) {
    var blockinput = event.parentElement;
    blockinput.classList.remove("block-input-active");
}