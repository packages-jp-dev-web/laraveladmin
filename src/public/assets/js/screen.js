$(document).ready(function () {
    $(document).on("click", '[data-toggle="collapse-jp"]', function () {
        var myThis = this;
        if ($(myThis.getAttribute("data-target")).hasClass("show-jp")) {
            $(myThis.getAttribute("data-target")).removeClass("show-jp");
        } else {
            $(myThis.getAttribute("data-target")).addClass("show-jp");
        }
    });
    $(document).on("click", ".button-drop", function () {
        var myThis = this;
        if ($(myThis.getAttribute("data-target")).hasClass("show-jp")) {
            $(myThis.getAttribute("data-target")).removeClass("show-jp");
            $(this).removeClass("is-active");
        } else {
            $(myThis.getAttribute("data-target")).addClass("show-jp");
            $(this).addClass("is-active");
            //this.children[0].textContent = 'expand_more';
        }
    });

    $(document).on("click", ".open-menu", function () {
        openOrCloseMenu();
    });
    $(document).on("focusin", "input", function () {
        focusInInput(this);
    });
    $(document).on("focusout", "input", function () {
        focusOutInput(this);
    });
    $(document).on("focusin", "textarea", function () {
        focusInInput(this);
    });
    $(document).on("focusout", "textarea", function () {
        focusOutInput(this);
    });
    $(document).on("focusin", "select", function () {
        focusInInput(this);
    });
    $(document).on("focusout", "select", function () {
        focusOutInput(this);
    });
    //Input File
    $(document).on('change', ':file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        var labelCustom = input.siblings('.custom-file-label');
        if (labelCustom.length) {
            if (numFiles > 0) {
                labelCustom.html(numFiles + ' arq(s): ' + label);
            } else {
                labelCustom.html('');
            }
        }
    });
});
