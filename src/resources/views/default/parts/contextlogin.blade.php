<div class="col-sm-8 col-md-6 col-lg-4 mx-auto">
    <div class=" cd--two">
        <div class=" cd-header">
            <h1>Login</h1>
        </div>
        <div class=" form-default">
            <form class="px-2">
                <div class=" block-input">
                    <input name="email" type="email" placeholder="Endereço de email">
                </div>
                <p class=" c-danger font-secoundary--small">Exemplo de mensagem de validação.</p>
                <div class=" block-input">
                    <input name="password" type="password" placeholder="Senha">
                </div>
                <p class=" c-danger font-secoundary--small">Exemplo de mensagem de validação.</p>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="remember">
                    <label class="custom-control-label" for="remember">Lembrar-me</label>
                </div>
                <button type="submit" class="mt-2 bt--pr--md"> Entrar </button>
                <div class="mt-2">
                    <a href="#">Esqueceu sua senha?</a>
                </div>
            </form>
        </div>
    </div>
</div>