<div class="col-sm-8 mx-auto">
    <div class=" cd--two">
        <div class=" cd-header">
            <h1>Error 404</h1>
        </div>
        <div class=" font-third--big">
            <h1>Recurso não encontrado, voltar ao <a href="{{url('/laraveladminhome')}}">home</a>.</h1>
        </div>
    </div>
</div>