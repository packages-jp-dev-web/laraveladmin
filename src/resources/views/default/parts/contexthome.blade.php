<div>
    <div class="row my-3">
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--one">
                <div class="cd-header">
                    <h1>Card .cd--one</h1>
                </div>
                <div class="m-3">
                    <h3>
                        Subscription
                    </h3>
                    <hr>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid vero accusantium minima neque
                        soluta
                        repudiandae nemo tempora, odio doloribus similique aperiam exercitationem consequuntur
                        cupiditate
                        suscipit consectetur iste! Vel, ea totam?
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--two">
                <div class="cd-header">
                    <h1>Card .cd--two</h1>
                </div>
                <div class="m-3">
                    <h3>
                        Subscription
                    </h3>
                    <hr>
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error est assumenda mollitia, eveniet
                        dolorum
                        suscipit sequi! Architecto eius vitae officia aperiam distinctio sequi, quibusdam, temporibus
                        dicta
                        fuga
                        omnis enim veniam!<br>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--three">
                <div class="cd-header">
                    <h1>Card .cd--three</h1>
                </div>
                <div class="m-3">
                    <h3>
                        Subscription
                    </h3>
                    <hr>
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cupiditate debitis aperiam repellat
                        ullam
                        possimus atque, exercitationem vero ratione magnam non qui voluptas architecto, laborum eos
                        ipsam
                        itaque! Voluptates, suscipit autem!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>