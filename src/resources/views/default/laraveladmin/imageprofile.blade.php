<div class="btn-group">
    <button class="remove-style-button " type="button" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <img src="{{url('imagessite/utilities/user2.png')}}">
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item align-items-center" href="#">
            <i class="far fa-id-card mr-2"></i>Profile
        </a>
        <a class="dropdown-item align-items-center " href="#"> <i class="fas fa-sign-out-alt mr-2"></i>Sair
        </a>
    </div>
</div>