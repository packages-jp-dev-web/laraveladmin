<div class="btn-group">
    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fa fa-dollar-sign"></i>
        <div class="badge">
            0
        </div>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="#">Nenhum orçamento</a>
    </div>
</div>
<div class="btn-group">
    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fas fa-comment-alt"></i>
        <div class="badge">
            110
        </div>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="#">Nenhuma mensagem</a>
    </div>
</div>
<div class="btn-group">
    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fas fa-bell"></i>
        <div class="badge">
            0
        </div>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="#">Nenhuma notificação</a>
    </div>
</div>
