<div class="context-item-sidebar">
    <a href="{{url('laraveladminhome')}}" class="item-sidebar-active text-white">
        <i class="fas fa-home "></i>Painel Administrativo
    </a>
</div>
<div class="context-item-sidebar">
    <div class="item-sidebar">
        <i class="fas fa-bug"></i>Errors
        <button class="hamburger hamburger--minus button-drop" type="button" data-target="#drop-table">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
    </div>
    <div id="drop-table" class="drop-item-sidebar collapse-jp">
        <li>
            <a href="{{url('/laraveladminerror')}}"> Error 404 </a><br>
        </li>
        <li>
            <a href="#"> Error 500 </a><br>
        </li>
    </div>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladmintypographys')}}" class="item-sidebar">
        <i class="fa fa-font"></i>Tipografia
    </a>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladmintabels')}}" class="item-sidebar">
        <i class="fa fa-table"></i>Tabelas
    </a>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladmincards')}}" class="item-sidebar">
        <i class="fa fa-layer-group"></i>Cards
    </a>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladminforms')}}" class="item-sidebar">
        <i class="far fa-keyboard"></i>Forms
    </a>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladminbuttons')}}" class="item-sidebar">
        <i class="fa fa-mouse-pointer"></i>Botões
    </a>
</div>
<div class="context-item-sidebar">
    <a href="{{url('laraveladminlogin')}}" class="item-sidebar">
        <i class="fa fa-sign-in-alt"></i>Login
    </a>
</div>
