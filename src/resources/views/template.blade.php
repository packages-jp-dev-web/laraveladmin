<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>
        @section('title') {{env('APP_NAME')}} @show
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    @if(view()->exists('laraveladmin.metas'))
    @include('laraveladmin.metas')
    @endif
    @include('laraveladmin::parts.css-default')
    @if(view()->exists('laraveladmin.cssdefault'))
    @include('laraveladmin.cssdefault')
    @endif
    @section('css-datable')
    @show
    @section('css')
    @show
</head>

<body>
    @section('contextgeral')
    <div class="site-context">
        <div class="context-sidebar">
            <div class="sidebar-background"></div>
            @section('sidebar')
            @include('laraveladmin::parts.sidebar')
            @show
        </div>
        <div class="context-site">
            @section('header')
            @include('laraveladmin::parts.header')
            @show
            <div class="p-2">
                @section('context')
                @show
            </div>
            @section('footer')
            @include('laraveladmin::parts.footer')
            @show
        </div>
    </div>
    @show
    @section('modal')
    @show
    @include('laraveladmin::parts.js-default')
    @if(view()->exists('laraveladmin.jsdefault'))
    @include('laraveladmin.jsdefault')
    @endif
    @section('js-datable')
    @show
    @section('js')
    @show
    <script type="text/javascript">
        $(document).ready(function() {
            @section('js-util')
            @show  
        } );
    </script>
</body>

</html>