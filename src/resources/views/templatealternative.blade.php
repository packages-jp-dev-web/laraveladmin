<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @section('title') {{env('APP_NAME')}} @show
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    @if(view()->exists('laraveladmin.metas'))
    @include('laraveladmin.metas')
    @endif
    @include('laraveladmin::parts.css-default')
    @section('css-datable')
    @show
    @section('css')
    @show
</head>

<body>
    <div class="block-template-alternative">
        @section('context')
        @show
    </div>


    @include('laraveladmin::parts.js-default')
    @section('js-datable')
    @show
    @section('js')
    @show
    <script type="text/javascript">
        $(document).ready(function() {
            @section('js-util')
            @show  
        } );
    </script>
</body>

</html>