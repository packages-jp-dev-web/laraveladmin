<div class="my-3">
    <p class="font-primary--big">Color: <span class="c-primary">.c-primary</span>, <span
            class=" c-secoundary">.c-secoundary</span>, <span class="c-third">.c-third</span>, <span
            class="c-success">.c-success</span> e <span class="c-danger">.c-danger</span></p>
    <h1 class="font-primary"> Primeira Fonte</h1>
    <h1 class="font-primary d-inline-block"> H1 </h1>
    <h2 class="font-primary d-inline-block"> H2 </h2>
    <h3 class="font-primary d-inline-block"> H3 </h3>
    <h4 class="font-primary d-inline-block"> H4 </h4>
    <h5 class="font-primary d-inline-block"> H5 </h5>
    <h6 class="font-primary d-inline-block"> H6 </h6>
    <p class="font-primary font-weight-bold">.font-primary, .font-primary--small, .font-primary--medium e
        .font-primary--big </p>
    <p class="font-primary--small c-danger">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-primary--medium c-success">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis
        rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-primary--big c-primary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>

    <h1 class="font-secoundary">Segundar Fonte</h1>
    <h1 class="font-secoundary d-inline-block"> H1 </h1>
    <h2 class="font-secoundary d-inline-block"> H2 </h2>
    <h3 class="font-secoundary d-inline-block"> H3 </h3>
    <h4 class="font-secoundary d-inline-block"> H4 </h4>
    <h5 class="font-secoundary d-inline-block"> H5 </h5>
    <h6 class="font-secoundary d-inline-block"> H6 </h6>
    <p class="font-secoundary font-weight-bold">.font-secoundary, .font-secoundary--small, .font-secoundary--medium e
        .font-secoundary--big </p>
    <p class="font-secoundary--small c-danger">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-secoundary--medium c-success">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis
        rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-secoundary--big c-primary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>

    <h1 class="font-third">Terceira Fonte</h1>
    <h1 class="font-third d-inline-block"> H1 </h1>
    <h2 class="font-third d-inline-block"> H2 </h2>
    <h3 class="font-third d-inline-block"> H3 </h3>
    <h4 class="font-third d-inline-block"> H4 </h4>
    <h5 class="font-third d-inline-block"> H5 </h5>
    <h6 class="font-third d-inline-block"> H6 </h6>
    <p class="font-third font-weight-bold">.font-third, .font-third--small, .font-third--medium e
        .font-third--big </p>
    <p class="font-third--small c-danger">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-third--medium c-success">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis
        rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-third--big c-primary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>

    <h1 class="font-fourth">Quarta Fonte</h1>
    <h1 class="font-fourth d-inline-block"> H1 </h1>
    <h2 class="font-fourth d-inline-block"> H2 </h2>
    <h3 class="font-fourth d-inline-block"> H3 </h3>
    <h4 class="font-fourth d-inline-block"> H4 </h4>
    <h5 class="font-fourth d-inline-block"> H5 </h5>
    <h6 class="font-fourth d-inline-block"> H6 </h6>
    <p class="font-third font-weight-bold">.font-fourth, .font-fourth--small, .font-fourth--medium e
        .font-fourth--big </p>
    <p class="font-fourth--small c-danger">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-fourth--medium c-success">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis
        rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
    <p class="font-fourth--big c-primary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga cumque
        reiciendis rerum
        inventore quia quo
        sequi error assumenda ea? Provident, illum repellendus! Consectetur doloribus aliquam debitis vitae est,
        deleniti hic.</p>
</div>