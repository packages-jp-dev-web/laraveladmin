<div class="p-2">
    <h1 class=" font-primary">
        Botões com a primeira cor
    </h1>
    <button class="bt--pr">.bt--pr</button>
    <button class="bt--pr--sm">.bt--pr--sm</button>
    <button class="bt--pr--md">.bt--pr--md</button>
    <button class="bt--pr--lg">.bt--pr--lg</button>
    <h1 class=" font-primary">
        Botões com a segunda cor
    </h1>
    <button class="bt--se">.bt--pr</button>
    <button class="bt--se--sm">.bt--pr--sm</button>
    <button class="bt--se--md">.bt--pr--md</button>
    <button class="bt--se--lg">.bt--pr--lg</button>
    <h1 class=" font-primary">
        Botões com a terceira cor
    </h1>
    <button class="bt--th">.bt--pr</button>
    <button class="bt--th--sm">.bt--pr--sm</button>
    <button class="bt--th--md">.bt--pr--md</button>
    <button class="bt--th--lg">.bt--pr--lg</button>

    <h1 class=" font-primary">
        Links com a primeira cor
    </h1>
    <a class="bt--pr">.bt--pr</a>
    <a class="bt--pr--sm">.bt--pr--sm</a>
    <a class="bt--pr--md">.bt--pr--md</a>
    <a class="bt--pr--lg">.bt--pr--lg</a>
    <h1 class=" font-primary">
        Links com a segunda cor
    </h1>
    <a class="bt--se">.bt--pr</a>
    <a class="bt--se--sm">.bt--pr--sm</a>
    <a class="bt--se--md">.bt--pr--md</a>
    <a class="bt--se--lg">.bt--pr--lg</a>
    <h1 class=" font-primary">
        Links com a terceira cor
    </h1>
    <a class="bt--th">.bt--pr</a>
    <a class="bt--th--sm">.bt--pr--sm</a>
    <a class="bt--th--md">.bt--pr--md</a>
    <a class="bt--th--lg">.bt--pr--lg</a>
</div>