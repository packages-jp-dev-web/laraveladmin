<form>
    <div class="row align-items-end">
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                <input type="text" placeholder="Nome">
            </div>
        </div>
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                <input type="email" placeholder="Email">
            </div>
        </div>
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                <input type="email" placeholder="Telefone">
            </div>
        </div>
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                <textarea placeholder="Descrição do usuário"></textarea>
            </div>
        </div>
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                {{Form::select('sex',['Masculino','Feminino'],null,[])}}
            </div>
        </div>
        <div class="col-sm-6 px-2">
            <div class=" block-input">
                <div class="custom-file">
                    <input type="file" name="image" class="custom-file-input" id="customFile">
                    <label class="custom-file-label mb-0" for="customFile" data-browse="Procurar"> Escolha uma
                        imagem </label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <button class="mt-3 bt--pr" type="submit"> Enviar</button>
        </div>
    </div>
</form>