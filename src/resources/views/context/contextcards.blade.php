<div>
    <div class="row my-3">
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--one">
                <div class="cd-header">
                    <h1>Card .cd--one(H1)</h1>
                    <h2>Card .cd--one(H2)</h2>
                    <h3>Card .cd--one(H3)</h3>
                    <h4>Card .cd--one(H4)</h4>
                    <h5>Card .cd--one(H5)</h5>
                    <h6>Card .cd--one(H6)</h6>

                </div>
                <div class="m-3">
                    <h1>Subscription(H1)</h1>
                    <h2>Subscription(H2)</h2>
                    <h3>Subscription(H3)</h3>
                    <h4>Subscription(H4)</h4>
                    <h5>Subscription(H5)</h5>
                    <h6>Subscription(H6)</h6>

                    <hr>
                    <p>
                        {{'<p></p>'}}<br>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid vero accusantium minima neque
                        soluta
                        repudiandae nemo tempora, odio doloribus similique aperiam exercitationem consequuntur
                        cupiditate
                        suscipit consectetur iste! Vel, ea totam?
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--two">
                <div class="cd-header">
                    <h1>Card .cd--one(H1)</h1>
                    <h2>Card .cd--one(H2)</h2>
                    <h3>Card .cd--one(H3)</h3>
                    <h4>Card .cd--one(H4)</h4>
                    <h5>Card .cd--one(H5)</h5>
                    <h6>Card .cd--one(H6)</h6>

                </div>
                <div class="m-3">
                    <h1>Subscription(H1)</h1>
                    <h2>Subscription(H2)</h2>
                    <h3>Subscription(H3)</h3>
                    <h4>Subscription(H4)</h4>
                    <h5>Subscription(H5)</h5>
                    <h6>Subscription(H6)</h6>

                    <hr>
                    <p>
                        {{'<p></p>'}}<br>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error est assumenda mollitia, eveniet
                        dolorum
                        suscipit sequi! Architecto eius vitae officia aperiam distinctio sequi, quibusdam, temporibus
                        dicta
                        fuga
                        omnis enim veniam!
                    </p>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 my-2">
            <div class="cd--three">
                <div class="cd-header">
                    <h1>Card .cd--one(H1)</h1>
                    <h2>Card .cd--one(H2)</h2>
                    <h3>Card .cd--one(H3)</h3>
                    <h4>Card .cd--one(H4)</h4>
                    <h5>Card .cd--one(H5)</h5>
                    <h6>Card .cd--one(H6)</h6>

                </div>
                <div class="m-3">
                    <h1>Subscription(H1)</h1>
                    <h2>Subscription(H2)</h2>
                    <h3>Subscription(H3)</h3>
                    <h4>Subscription(H4)</h4>
                    <h5>Subscription(H5)</h5>
                    <h6>Subscription(H6)</h6>

                    <hr>
                    <p>
                        {{'<p></p>'}}<br>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cupiditate debitis aperiam repellat
                        ullam
                        possimus atque, exercitationem vero ratione magnam non qui voluptas architecto, laborum eos
                        ipsam
                        itaque! Voluptates, suscipit autem!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>