<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-default">
            <div class="cd--two">
                <div class="cd-header">
                    <h3 class="modal-defautl-title">

                    </h3>
                    <button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
                <p class="my-4 modal-defautl-content"></p>

                <a id="button-delete-modal" href="#" class=" bt--pr--md mr-3">Sim</a>
                <button type="button" class=" bt--th--md ml-3" data-dismiss="modal">Não</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>