<section class="header">
    <div class="context-header">
        <div class="row justify-content-end">
            <div class="col header-text">
                @if(view()->exists('laraveladmin.bannerheader'))
                @include('laraveladmin.bannerheader')
                @else
                Banner Header
                @endif
            </div>
            <div class="header-nav">
                @if(view()->exists('laraveladmin.headernav'))
                @include('laraveladmin.headernav')
                @else
                <div class="btn-group">
                    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-dollar-sign"></i>
                        <div class="badge">
                            0
                        </div>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Nenhum orçamento</a>
                    </div>
                </div>
                <div class="btn-group">
                    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-comment-alt"></i>
                        <div class="badge">
                            110
                        </div>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Nenhuma mensagem</a>
                    </div>
                </div>
                <div class="btn-group">
                    <button class="remove-style-button item-nav" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell"></i>
                        <div class="badge">
                            0
                        </div>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Nenhuma notificação</a>
                    </div>
                </div>
                @endif
                @if(view()->exists('laraveladmin.imageprofile'))
                @include('laraveladmin.imageprofile')
                @else
                <div class="btn-group">
                    <button class="remove-style-button " type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <img src="{{url('imageslaraveladmin/utilities/user2.png')}}">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item align-items-center" href="#">
                            <i class="far fa-id-card mr-2"></i>Profile
                        </a>
                        <a class="dropdown-item align-items-center " href="#"> <i
                                class="fas fa-sign-out-alt mr-2"></i>Sair
                        </a>
                    </div>
                </div>
                @endif
            </div>
            <div class="header-menu">
                <button class="hamburger hamburger--arrowalt-r open-menu" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</section>
