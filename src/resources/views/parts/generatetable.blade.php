<div class="table-default">
    <table id="{{$datatable['idtable']}}" class="display responsive nowrap table table-hover row-border"
        style="width:100%">
        {!!$table!!}
    </table>
</div>

@section('modal')
@parent
@include('laraveladmin::parts.modal-delete')
@endsection
@section('js-datable')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
</script>
@stop

@section('css-datable')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">-->
@stop
@section('js-util')
@parent
@if(isset($nobuttons))
{{$datatable['idtable']}} = $('#{{$datatable['idtable']}}').DataTable({
responsive: true,
"language": {
"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
}
});
@else
{{$datatable['idtable']}} = $('#{{$datatable['idtable']}}').DataTable({
responsive: true,
"columnDefs": [ {
"targets": [{{$lastindice}}],
"orderable": false,
} ],
"language": {
"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
}
});
@endif
@stop
