<?php

namespace LaravelAdmin;

use Illuminate\Support\ServiceProvider;

class LaravelAdminServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'laraveladmin');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->publishes([__DIR__ . "/resources/views/default" => base_path('resources/views/defaultlaraveladmin')], 'laraveladminpublish');
        $this->publishes([__DIR__ . "/public/assets/css/laraveladmin.min.css" => base_path('public/assets/css/laraveladmin.min.css')], 'laraveladminpublish');
        $this->publishes([__DIR__ . "/public/assets/scss/scssaux/variablesandutilities.scss" => base_path('public/assets/scss/laraveladminvariables.scss')], 'laraveladminpublish');
        $this->publishes([__DIR__ . "/public/assets/js/laraveladmin.min.js" => base_path('public/assets/js/laraveladmin.min.js')], 'laraveladminpublish');
        $this->publishes([__DIR__ . "/public/imageslaraveladmin/" => base_path('public/imageslaraveladmin/')], 'laraveladminpublish');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
